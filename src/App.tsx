import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { Badge, Card, Col, Layout, Row, Spin, Typography } from 'antd';
import { Content, Header } from 'antd/lib/layout/layout';
import Meta from 'antd/lib/card/Meta';
const { Text } = Typography;

function App() {
    const [topCats, setTopCats] = useState<any[]>([]);

    const [listings, setListings] = useState<any[]>([]);
    const [rarityData, setRarityData] = useState<any[]>([]);
    const [loading, setLoading] = useState(true);
    const [currentPage, setCurrentPage] = useState(0);
    const [totalPages, settotalPages] = useState(0);
    function rankSort(a: any, b: any) {
        if (a.rank < b.rank) {
            return 1;
        }
        if (a.rank > b.rank) {
            return -1;
        }
        return 0;
    }
    function priceSort(a: any, b: any) {
        if (a.listing_lovelace < b.listing_lovelace) {
            return 1;
        }
        if (a.listing_lovelace > b.listing_lovelace) {
            return -1;
        }
        return 0;
    }
    useEffect(() => {
        setLoading(true);
        const initCats = async () => {
            const rarityJson: any[] = await fetch('rarity/voxcatsrank.json', {
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json'
                }
            }).then(async (response) => {
                const result: any[] = await response.json();
                // const top1kCats = result.filter((x) => x.rank > 0 && x.rank <= 1000).sort(rankSort);
                // setTopCats(top1kCats);
                return result;
            });

            const fetchListings = async () => {
                const jpgAssets: any = await fetch(
                    'https://server.jpgstoreapis.com/search/tokens?policyIds=[%22a8c68ff1e130c79cd0cad73357670b3b4fd11a0708319a99be14e860%22]&saleType=buy-now&sortBy=price-low-to-high&traits=%7B%7D&nameQuery=&verified=default&pagination=%7B%7D&size=4000'
                ).then((r) => r.json());
                const tokens = jpgAssets.tokens
                    .map((item: any) => {
                        item.rank = rarityJson.find((x) => x.serial === item.asset_num)?.rank;
                        return item;
                    })
                    .filter((x: any) => x.rank > 0 && x.rank <= 2000);

                setListings(tokens);
                setLoading(false);
            };
            fetchListings();
        };
        initCats();
    }, []);

    return (
        <Layout>
            {/* <Header className="site-layout-sub-header-background" style={{ padding: 0 }}>
               
            </Header> */}
            <div style={{ width: '100%', textAlign: 'center' }}>
                <img src={process.env.PUBLIC_URL + '/vox_logo.png'} alt="" />
            </div>
            <Content>
                <div className="" style={{ minHeight: 1024, textAlign: 'center', padding: '20px' }}>
                    <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        {loading && <div style={{ width: '100%', textAlign: 'center', marginTop: '20%' }}>Loading...</div>}
                        {!loading &&
                            listings.map((item) => {
                                return (
                                    <Col xs={8} sm={8} md={6} lg={4} xl={4} style={{ marginBottom: '12px' }}>
                                        <Badge.Ribbon text="For sale" color="red">
                                            <Card
                                                size="small"
                                                cover={
                                                    <img
                                                        // src={'https://ipfs.blockfrost.dev/ipfs/' + item.asset.metadata.image.replace('ipfs://', '')}
                                                        // src={'https://bosscatrocketclub.com/media/bcrc' + item.serial + '.jpg'}
                                                        src={
                                                            'https://voxcats.bossplanet.games/cardano/media/vox_cat_' +
                                                            item.display_name.split('#')[1] +
                                                            '.jpg'
                                                        }
                                                    />
                                                }>
                                                <Meta
                                                    title={<div style={{ fontSize: '13px' }}>{<Text ellipsis>{item.display_name}</Text>}</div>}
                                                    description={
                                                        <>
                                                            <div style={{ color: 'black', fontSize: '13px' }}>
                                                                <Text ellipsis>
                                                                    <div style={{ marginBottom: '8px' }}>
                                                                        Rank: <strong>{item.rank}</strong>
                                                                    </div>
                                                                </Text>
                                                            </div>
                                                            <div style={{ color: 'black', fontSize: '13px' }}>
                                                                <Text ellipsis>
                                                                    <div style={{ marginBottom: '8px' }}>
                                                                        Price: <strong>{parseInt(item.listing_lovelace) / 1000000} ADA</strong>
                                                                    </div>
                                                                </Text>
                                                            </div>
                                                        </>
                                                    }
                                                />
                                            </Card>
                                        </Badge.Ribbon>
                                    </Col>
                                );
                            })}
                    </Row>
                    {/* {loading ? (
                        'Loading... ' + currentPage + '/' + totalPages
                    ) : (
                        <>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                {listings.map((item: any) => {
                                    return (
                                        <Col xs={8} sm={8} md={6} lg={4} xl={4} style={{ marginBottom: '12px' }}>
                                            <Card
                                                size="small"
                                                cover={
                                                    <img
                                                        src={'https://ipfs.blockfrost.dev/ipfs/' + item.asset.metadata.image.replace('ipfs://', '')}
                                                    />
                                                }
                                                actions={[
                                                    <a target="_blank" href={'https://cnft.io/token/' + item._id}>
                                                        Buy
                                                    </a>
                                                ]}>
                                                <Meta
                                                    title={<div style={{ fontSize: '13px' }}>{<Text ellipsis>{item.metadata['name']}</Text>}</div>}
                                                    description={
                                                        <div style={{ color: 'black', fontSize: '13px' }}>
                                                            <Text ellipsis>
                                                                <div style={{ marginBottom: '8px' }}>
                                                                    <strong>{item.rank}</strong>
                                                                </div>
                                                            </Text>

                                                            <div>Rank: {item.rank}</div>
                                                            <div>
                                                                <strong>{item.price / 1000000} ₳</strong>
                                                            </div>
                                                        </div>
                                                    }
                                                />
                                            </Card>
                                        </Col>
                                    );
                                })}
                            </Row>
                        </>
                    )} */}
                </div>
            </Content>
        </Layout>
    );
}

export default App;
